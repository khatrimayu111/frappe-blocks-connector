export const SETUP_ALREADY_COMPLETE = 'Setup already complete';
export const PLEASE_RUN_SETUP = 'Please run setup';
export const SOMETHING_WENT_WRONG = 'Something went wrong';
export const COMMUNICATION_SERVICE_NOT_IMPLEMENTED =
  'Communication Server not implemented';
export const INVALID_STATE = 'Invalid State';
export const INVALID_FRAPPE_CLIENT = 'Invalid Frappe Client';
export const INVALID_USER = 'Invalid User';
export const INVALID_FRAPPE_TOKEN = 'Invalid Frappe Token';
export const NOT_CONNECTED = 'not connected';
