export const ADMINISTRATOR = 'administrator';
export const TOKEN = 'token';
export const AUTHORIZATION = 'authorization';
export const SERVICE = 'frappe-connector';
export const PUBLIC = 'public';
export const APP_NAME = 'frappe-connector';
export const SWAGGER_ROUTE = 'api-docs';
export const COMMUNICATION_SERVER = 'communication-server';
export enum ConnectedServices {
  CommunicationServer = 'communication-server',
  InfrastructureConsole = 'infrastructure-console',
  IdentityProvider = 'identity-provider',
}
export const BEARER_HEADER_VALUE_PREFIX = 'Bearer';
export const APPLICATION_JSON_CONTENT_TYPE = 'application/json';
export const CONTENT_TYPE_HEADER_KEY = 'Content-Type';
