import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { FrappeClientUpdatedEvent } from './frappe-client-updated.event';

@EventsHandler(FrappeClientUpdatedEvent)
export class FrappeClientUpdatedHandler
  implements IEventHandler<FrappeClientUpdatedEvent> {
  handle(event: FrappeClientUpdatedEvent) {
    const { client: provider } = event;
    provider
      .save()
      .then(success => {})
      .catch(error => {});
  }
}
