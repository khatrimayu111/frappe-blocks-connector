import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { FrappeClientRemovedHandler } from './frappe-client-removed.handler';
import { FrappeClientRemovedEvent } from './frappe-client-removed.event';
import { FrappeClient } from '../../entities/frappe-client/frappe-client.entity';

describe('Event: FrappeClientRemovedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: FrappeClientRemovedHandler;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        FrappeClientRemovedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<FrappeClientRemovedHandler>(
      FrappeClientRemovedHandler,
    );
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should remove OAuth2Provider', async () => {
    const mockProvider = new FrappeClient();
    eventBus$.publish = jest.fn(() => {});
    mockProvider.remove = jest.fn(() => Promise.resolve(mockProvider));
    await eventHandler.handle(new FrappeClientRemovedEvent(mockProvider));
    expect(mockProvider.remove).toHaveBeenCalledTimes(1);
  });
});
