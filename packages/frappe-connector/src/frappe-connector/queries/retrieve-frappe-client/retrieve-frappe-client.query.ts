import { IQuery } from '@nestjs/cqrs';

export class RetrieveFrappeClientQuery implements IQuery {
  constructor(public readonly uuid: string) {}
}
