import { ListFrappeClientHandler } from './list-frappe-client/list-frappe-client.handler';
import { RetrieveFrappeClientHandler } from './retrieve-frappe-client/retrieve-frappe-client.handler';
import { VerifyClientConnectionHandler } from './verify-client-connection/verify-client-connection.handler';
import { ListRequestAdminLogHandler } from './list-request-log/list-request-log.handler';
import { RetrieveRequestLogHandler } from './retrieve-request-log/retrieve-request-log.handler';
import { ListRequestUserLogHandler } from './list-user-request-log/list-user-request-log.handler';

export const FrappeQueryHandlers = [
  ListFrappeClientHandler,
  RetrieveFrappeClientHandler,
  VerifyClientConnectionHandler,
  ListRequestAdminLogHandler,
  RetrieveRequestLogHandler,
  ListRequestUserLogHandler,
];
