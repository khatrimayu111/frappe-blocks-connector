import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { ListFrappeTokenQuery } from './list-frappe-token.query';
import { FrappeTokenManagerService } from '../../aggregates/frappe-token-manager/frappe-token-manager.service';

@QueryHandler(ListFrappeTokenQuery)
export class ListFrappeTokenHandler
  implements IQueryHandler<ListFrappeTokenQuery> {
  constructor(private manager: FrappeTokenManagerService) {}

  async execute(query: ListFrappeTokenQuery) {
    const { offset, limit, search, sort } = query;
    return await this.manager.list(offset, limit, search, sort);
  }
}
