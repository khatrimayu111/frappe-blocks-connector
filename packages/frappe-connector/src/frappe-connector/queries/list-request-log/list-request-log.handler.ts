import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { ListAdminRequestLogQuery } from './list-request-log.query';
import { RequestLogManagerService } from '../../aggregates/request-log-manager/request-log-manager.service';

@QueryHandler(ListAdminRequestLogQuery)
export class ListRequestAdminLogHandler
  implements IQueryHandler<ListAdminRequestLogQuery> {
  constructor(private manager: RequestLogManagerService) {}

  async execute(query: ListAdminRequestLogQuery) {
    const { offset, limit, search, sort } = query;
    return await this.manager.list(offset, limit, search, sort);
  }
}
