import { Test, TestingModule } from '@nestjs/testing';
import { FrappeTokenService } from './frappe-token.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { FrappeToken } from './frappe-token.entity';

describe('FrappeTokenService', () => {
  let service: FrappeTokenService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FrappeTokenService,
        { provide: getRepositoryToken(FrappeToken), useValue: {} },
      ],
    }).compile();

    service = module.get<FrappeTokenService>(FrappeTokenService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
