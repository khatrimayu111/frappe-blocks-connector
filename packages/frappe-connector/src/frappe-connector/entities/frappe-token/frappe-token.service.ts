import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { FrappeToken } from './frappe-token.entity';

@Injectable()
export class FrappeTokenService {
  constructor(
    @InjectRepository(FrappeToken)
    private readonly frappeTokenRepository: MongoRepository<FrappeToken>,
  ) {}

  async save(params) {
    return await this.frappeTokenRepository.save(params);
  }

  async find(): Promise<FrappeToken[]> {
    return await this.frappeTokenRepository.find();
  }

  async findOne(params): Promise<FrappeToken> {
    return await this.frappeTokenRepository.findOne(params);
  }

  async update(query, params) {
    return await this.frappeTokenRepository.update(query, params);
  }

  async count() {
    return await this.frappeTokenRepository.count();
  }

  async paginate(skip: number, take: number) {
    return await this.frappeTokenRepository.find({ skip, take });
  }

  async deleteMany(params) {
    return await this.frappeTokenRepository.deleteMany(params);
  }

  async list(skip: number, take: number) {
    const providers = await this.frappeTokenRepository.find({ skip, take });

    return {
      docs: providers,
      length: await this.frappeTokenRepository.count(),
      offset: skip,
    };
  }
}
