import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { FrappeClient } from './frappe-client.entity';

@Injectable()
export class FrappeClientService {
  constructor(
    @InjectRepository(FrappeClient)
    private readonly frappeClientRepository: MongoRepository<FrappeClient>,
  ) {}

  async save(params) {
    return await this.frappeClientRepository.save(params);
  }

  async find(): Promise<FrappeClient[]> {
    return await this.frappeClientRepository.find();
  }

  async findOne(params): Promise<FrappeClient> {
    return await this.frappeClientRepository.findOne(params);
  }

  async update(query, params) {
    return await this.frappeClientRepository.update(query, params);
  }

  async count() {
    return await this.frappeClientRepository.count();
  }

  async list(skip: number, take: number) {
    const providers = await this.frappeClientRepository.find({ skip, take });

    providers.map(client => {
      delete client.clientSecret;
    });

    return {
      docs: providers,
      length: await this.frappeClientRepository.count(),
      offset: skip,
    };
  }
}
