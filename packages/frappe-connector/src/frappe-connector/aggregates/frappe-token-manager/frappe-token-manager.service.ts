import {
  Injectable,
  HttpService,
  NotImplementedException,
  BadRequestException,
  HttpStatus,
  NotFoundException,
} from '@nestjs/common';
import * as uuidv4 from 'uuid/v4';
import { switchMap, map } from 'rxjs/operators';
import { throwError, of, from } from 'rxjs';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import {
  COMMUNICATION_SERVICE_NOT_IMPLEMENTED,
  INVALID_STATE,
  INVALID_FRAPPE_CLIENT,
} from '../../../constants/messages';
import { RequestStateService } from '../../entities/request-state/request-state.service';
import { FrappeTokenService } from '../../entities/frappe-token/frappe-token.service';
import { RequestState } from '../../entities/request-state/request-state.entity';
import { FrappeClientService } from '../../entities/frappe-client/frappe-client.service';
import { stringify } from 'querystring';
import { FrappeClient } from '../../entities/frappe-client/frappe-client.entity';

export const REDIRECT_ENDPOINT = '/frappe/callback';

@Injectable()
export class FrappeTokenManagerService {
  private client: FrappeClient;
  private localState = new RequestState();

  constructor(
    private readonly http: HttpService,
    private readonly settings: SettingsService,
    private readonly requestState: RequestStateService,
    private readonly frappeToken: FrappeTokenService,
    private readonly frappeClient: FrappeClientService,
  ) {}

  connectClientForUser(uuid: string, redirect: string, req) {
    return this.settings.find().pipe(
      switchMap(settings => {
        if (!settings.communicationService) {
          return throwError(
            new NotImplementedException(COMMUNICATION_SERVICE_NOT_IMPLEMENTED),
          );
        }

        return from(this.frappeClient.findOne({ uuid })).pipe(
          switchMap(client => {
            if (!client)
              return throwError(new NotFoundException(INVALID_FRAPPE_CLIENT));
            this.client = client;
            return from(
              this.requestState.save({
                uuid: uuidv4(),
                redirect,
                userUuid: req.token.sub,
                providerUuid: uuid,
                creation: new Date(),
              }),
            );
          }),
          switchMap(state => {
            const encodedState = state.uuid;
            let redirectTo =
              this.client.authorizationURL +
              '?client_id=' +
              this.client.clientId;
            redirectTo +=
              '&redirect_uri=' +
              encodeURIComponent(settings.appURL + REDIRECT_ENDPOINT);
            redirectTo += '&scope=' + this.client.scope.join('%20');
            redirectTo += '&response_type=code';
            redirectTo += '&state=' + encodedState;
            return of({ redirect: redirectTo });
          }),
        );
      }),
    );
  }

  processCode(code: string, state: string, res) {
    this.settings
      .find()
      .pipe(
        switchMap(settings => {
          return from(this.requestState.findOne({ uuid: state })).pipe(
            switchMap(requestState => {
              if (!requestState) {
                return throwError(new BadRequestException(INVALID_STATE));
              }

              this.localState = requestState;

              return from(
                this.frappeClient.findOne({
                  uuid: this.localState.providerUuid,
                }),
              ).pipe(
                switchMap(client => {
                  const requestBody = {
                    client_id: client.clientId,
                    code,
                    grant_type: 'authorization_code',
                    scope: client.scope.join('%20'),
                    client_secret: client.clientSecret,
                    redirect_uri: settings.appURL + REDIRECT_ENDPOINT,
                  };

                  return this.http.post(
                    client.tokenURL,
                    stringify(requestBody),
                    {
                      headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                      },
                    },
                  );
                }),
              );
            }),
          );
        }),
        map(response => response.data),
        switchMap(token => {
          return from(
            this.frappeToken.findOne({
              providerUuid: this.localState.providerUuid,
              userUuid: this.localState.userUuid,
            }),
          ).pipe(
            switchMap(localToken => {
              // Set Saved Token Expiration Time
              const expirationTime = new Date();
              expirationTime.setSeconds(
                expirationTime.getSeconds() + (token.expires_in || 3600),
              );

              if (!localToken) {
                return from(
                  this.frappeToken.save({
                    uuid: uuidv4(),
                    providerUuid: this.localState.providerUuid,
                    userUuid: this.localState.userUuid,
                    accessToken: token.access_token,
                    refreshToken: token.refresh_token,
                    idToken: token.id_token,
                    expirationTime,
                  }),
                );
              }

              this.revokeToken(
                localToken.accessToken,
                this.localState.providerUuid,
              );
              localToken.uuid = localToken.uuid;
              localToken.providerUuid = this.localState.providerUuid;
              localToken.userUuid = this.localState.userUuid;
              localToken.accessToken = token.access_token;
              localToken.refreshToken = token.refresh_token;
              localToken.idToken = token.id_token;
              localToken.expirationTime = expirationTime;
              return from(localToken.save());
            }),
          );
        }),
      )
      .subscribe({
        next: response => {
          const redirect = this.localState.redirect || '/';

          this.deleteRequestState(this.localState);

          return res.redirect(HttpStatus.FOUND, redirect);
        },
        error: error => {
          res.status(HttpStatus.INTERNAL_SERVER_ERROR);
          return res.json({ error: error.message });
        },
      });
  }

  revokeToken(accessToken: string, providerUuid: string) {
    from(this.frappeClient.findOne({ uuid: providerUuid }))
      .pipe(
        switchMap(client => {
          return this.http.get(client.revocationURL + '?token=' + accessToken);
        }),
      )
      .subscribe({
        next: success => {},
        error: error => {},
      });
  }

  deleteRequestState(requestState: RequestState) {
    from(requestState.remove()).subscribe({
      next: success => {},
      error: error => {},
    });
  }

  async verifyClientConnection(providerUuid: string, userUuid: string) {
    return await this.frappeToken.findOne({ userUuid, providerUuid });
  }

  async list(offset: number, limit: number, search?: string, sort?: string) {
    offset = Number(offset);
    limit = Number(offset);
    return this.frappeToken.list(offset, limit);
  }
}
