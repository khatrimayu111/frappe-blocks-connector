import { Test, TestingModule } from '@nestjs/testing';
import { HttpModule } from '@nestjs/common';
import { RequestLogManagerService } from './request-log-manager.service';
import { RequestLogService } from '../../../frappe-connector/entities/request-log/request-log.service';

describe('RequestLogManagerService', () => {
  let service: RequestLogManagerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        RequestLogManagerService,
        { provide: RequestLogService, useValue: {} },
      ],
    }).compile();

    service = module.get<RequestLogManagerService>(RequestLogManagerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
