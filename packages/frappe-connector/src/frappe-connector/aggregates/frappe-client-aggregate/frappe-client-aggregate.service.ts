import {
  Injectable,
  NotFoundException,
  HttpService,
  BadRequestException,
  UnauthorizedException,
  ForbiddenException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { stringify } from 'querystring';
import { switchMap, map, catchError } from 'rxjs/operators';
import { from, throwError } from 'rxjs';
import * as uuidv4 from 'uuid/v4';
import { FrappeClientDto } from '../../policies/frappe-client-dto/frappe-client.dto';
import { FrappeClientAddedEvent } from '../../events/frappe-client-added/frappe-client-added.event';
import { FrappeClientRemovedEvent } from '../../events/frappe-client-removed/frappe-client-removed.event';
import { FrappeClientService } from '../../entities/frappe-client/frappe-client.service';
import { FrappeClient } from '../../entities/frappe-client/frappe-client.entity';
import { FrappeTokenService } from '../../entities/frappe-token/frappe-token.service';
import { HttpMethod } from '../../../constants/request-methods';
import {
  INVALID_FRAPPE_CLIENT,
  INVALID_FRAPPE_TOKEN,
} from '../../../constants/messages';
import { FrappeToken } from '../../entities/frappe-token/frappe-token.entity';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { REDIRECT_ENDPOINT } from '../frappe-token-manager/frappe-token-manager.service';
import { RequestLog } from '../../entities/request-log/request-log.entity';

@Injectable()
export class FrappeClientAggregateService extends AggregateRoot {
  constructor(
    private readonly frappeClient: FrappeClientService,
    private readonly frappeToken: FrappeTokenService,
    private readonly http: HttpService,
    private readonly settings: ServerSettingsService,
  ) {
    super();
  }

  async addProvider(payload: FrappeClientDto) {
    const provider = Object.assign(new FrappeClient(), payload);
    this.apply(new FrappeClientAddedEvent(provider));
  }

  async removeProvider(uuid: string) {
    const provider = await this.frappeClient.findOne({ uuid });
    if (!provider) throw new NotFoundException({ uuid });
    this.apply(new FrappeClientRemovedEvent(provider));
  }

  async retrieveProvider(uuid: string) {
    const provider = await this.frappeClient.findOne({ uuid });
    if (!provider) throw new NotFoundException({ uuid });
    delete provider.clientSecret;
    return provider;
  }

  async updateProvider(uuid: string, payload: FrappeClientDto) {
    const provider = await this.frappeClient.findOne({ uuid });
    if (!provider) throw new NotFoundException({ uuid });
    const updatedProvider = Object.assign(provider, payload);
    this.apply(new FrappeClientAddedEvent(updatedProvider));
  }

  async list(offset: number, limit: number, search?: string, sort?: string) {
    offset = Number(offset);
    limit = Number(offset);
    return this.frappeClient.list(offset, limit);
  }

  async makeRequest(
    method: string,
    clientUuid: string,
    params: any,
    query: any,
    payload: any,
    userUuid: string,
  ) {
    const client = await this.frappeClient.findOne({ uuid: clientUuid });
    if (!client) throw new BadRequestException(INVALID_FRAPPE_CLIENT);
    let token = await this.frappeToken.findOne({
      providerUuid: clientUuid,
      userUuid,
    });
    if (!token) throw new UnauthorizedException(INVALID_FRAPPE_TOKEN);

    const requestLog = new RequestLog();
    requestLog.uuid = uuidv4();
    requestLog.userUuid = userUuid;
    requestLog.providerUuid = clientUuid;
    requestLog.creation = new Date();
    await requestLog.save();

    token = await this.getUserAccessToken(token, requestLog);
    const requestUrl =
      client.authServerURL + `/${params[0]}?` + stringify(query);

    const requestSubscription = {
      next: async success => {
        requestLog.successResponse = success.data;
        await requestLog.save();
      },
      error: async error => {
        requestLog.failResponse = error.message;
        await requestLog.save();
      },
    };

    const catchInvalidTokenError = catchError(error => {
      try {
        if (error.response.data.exc.includes('validate_oauth')) {
          token
            .remove()
            .then(res => {})
            .catch(err => {});
        }
      } catch (error) {}
      return throwError(error);
    });

    switch (method) {
      case HttpMethod.GET:
        this.http
          .get(requestUrl, {
            headers: { Authorization: 'Bearer ' + token.accessToken },
          })
          .pipe(catchInvalidTokenError)
          .subscribe(requestSubscription);
        break;

      case HttpMethod.POST:
        this.http
          .post(requestUrl, stringify(payload), {
            headers: { Authorization: 'Bearer ' + token.accessToken },
          })
          .pipe(catchInvalidTokenError)
          .subscribe(requestSubscription);
        break;

      case HttpMethod.PUT:
        this.http
          .put(requestUrl, stringify(payload), {
            headers: { Authorization: 'Bearer ' + token.accessToken },
          })
          .pipe(catchInvalidTokenError)
          .subscribe(requestSubscription);
        break;

      case HttpMethod.PATCH:
        this.http
          .patch(requestUrl, stringify(payload), {
            headers: { Authorization: 'Bearer ' + token.accessToken },
          })
          .pipe(catchInvalidTokenError)
          .subscribe(requestSubscription);
        break;

      case HttpMethod.DELETE:
        this.http
          .delete(requestUrl, {
            headers: { Authorization: 'Bearer ' + token.accessToken },
          })
          .pipe(catchInvalidTokenError)
          .subscribe(requestSubscription);
        break;
    }

    return { requestLog: requestLog.uuid };
  }

  async getUserAccessToken(frappeToken: FrappeToken, requestLog: RequestLog) {
    const frappeClient = await this.frappeClient.findOne({
      uuid: frappeToken.providerUuid,
    });
    const expiration = frappeToken.expirationTime;
    const settings = await this.settings.find();
    expiration.setSeconds(expiration.getSeconds() - 60);
    if (new Date() > expiration) {
      const requestBody = {
        grant_type: 'refresh_token',
        refresh_token: frappeToken.refreshToken,
        client_id: frappeClient.clientId,
        redirect_uri: settings.appURL + REDIRECT_ENDPOINT,
        // scope: frappeClient.scope.join('%20'),
      };
      return this.http
        .post(frappeClient.tokenURL, stringify(requestBody), {
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        })
        .pipe(
          map(res => res.data),
          catchError(err => {
            requestLog
              .remove()
              .then(success => {})
              .catch(error => {});

            this.revokeToken(frappeToken.accessToken, frappeClient);

            frappeToken
              .remove()
              .then(success => {})
              .catch(error => {});

            return throwError(new ForbiddenException(INVALID_FRAPPE_TOKEN));
          }),
          switchMap(bearerToken => {
            this.revokeToken(frappeToken.accessToken, frappeClient);

            frappeToken.accessToken = bearerToken.access_token;
            frappeToken.refreshToken = bearerToken.refresh_token;

            const expirationTime = new Date();
            expirationTime.setSeconds(
              expirationTime.getSeconds() + (bearerToken.expires_in || 3600),
            );

            frappeToken.expirationTime = expirationTime;
            return from(frappeToken.save());
          }),
        )
        .toPromise();
    } else {
      return frappeToken;
    }
  }

  revokeToken(accessToken: string, frappeClient: FrappeClient) {
    this.http
      .get(frappeClient.revocationURL + '?token=' + accessToken)
      .subscribe({
        next: success => {},
        error: error => {},
      });
  }
}
