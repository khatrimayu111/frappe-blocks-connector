import { ICommand } from '@nestjs/cqrs';
import { FrappeClientDto } from '../../policies/frappe-client-dto/frappe-client.dto';

export class UpdateFrappeClientCommand implements ICommand {
  constructor(
    public readonly providerUuid: string,
    public readonly payload: FrappeClientDto,
  ) {}
}
