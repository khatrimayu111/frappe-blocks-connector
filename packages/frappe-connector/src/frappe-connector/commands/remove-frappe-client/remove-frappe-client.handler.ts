import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveFrappeClientCommand } from './remove-frappe-client.command';
import { FrappeClientAggregateService } from '../../aggregates/frappe-client-aggregate/frappe-client-aggregate.service';

@CommandHandler(RemoveFrappeClientCommand)
export class RemoveFrappeClientHandler
  implements ICommandHandler<RemoveFrappeClientCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: FrappeClientAggregateService,
  ) {}

  async execute(command: RemoveFrappeClientCommand) {
    // TODO: record actorUuid from command
    const { providerUuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.removeProvider(providerUuid);
    aggregate.commit();
  }
}
