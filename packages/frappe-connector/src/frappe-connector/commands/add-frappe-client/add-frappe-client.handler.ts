import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AddFrappeClientCommand } from './add-frappe-client.command';
import { FrappeClientAggregateService } from '../../aggregates/frappe-client-aggregate/frappe-client-aggregate.service';

@CommandHandler(AddFrappeClientCommand)
export class AddFrappeClientHandler
  implements ICommandHandler<AddFrappeClientCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: FrappeClientAggregateService,
  ) {}

  async execute(command: AddFrappeClientCommand) {
    const { payload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addProvider(payload);
    aggregate.commit();
  }
}
