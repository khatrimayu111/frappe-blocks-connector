import { ICommand } from '@nestjs/cqrs';
import { FrappeClientDto } from '../../policies/frappe-client-dto/frappe-client.dto';

export class AddFrappeClientCommand implements ICommand {
  constructor(public readonly payload: FrappeClientDto) {}
}
