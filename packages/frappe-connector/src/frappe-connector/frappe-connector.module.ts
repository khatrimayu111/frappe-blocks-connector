import { Module, HttpModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FrappeController } from './controllers/frappe/frappe.controller';
import { FrappeTokenManagerService } from './aggregates/frappe-token-manager/frappe-token-manager.service';
import { FrappeTokenService } from './entities/frappe-token/frappe-token.service';
import { FrappeToken } from './entities/frappe-token/frappe-token.entity';
import { RequestStateService } from './entities/request-state/request-state.service';
import { RequestState } from './entities/request-state/request-state.entity';
import { FrappeClientService } from './entities/frappe-client/frappe-client.service';
import { FrappeClient } from './entities/frappe-client/frappe-client.entity';
import { FrappeClientAggregateService } from './aggregates/frappe-client-aggregate/frappe-client-aggregate.service';
import { FrappeCommandHandlers } from './commands';
import { FrappeEventHandlers } from './events';
import { FrappeQueryHandlers } from './queries';
import { CqrsModule } from '@nestjs/cqrs';
import { RequestLogService } from './entities/request-log/request-log.service';
import { RequestLog } from './entities/request-log/request-log.entity';
import { FrappeTokenController } from './controllers/frappe-token/frappe-token.controller';
import { RequestLogController } from './controllers/request-log/request-log.controller';
import { RequestLogManagerService } from './aggregates/request-log-manager/request-log-manager.service';

@Module({
  imports: [
    CqrsModule,
    HttpModule,
    TypeOrmModule.forFeature([
      FrappeToken,
      RequestState,
      FrappeClient,
      RequestLog,
    ]),
  ],
  controllers: [FrappeController, FrappeTokenController, RequestLogController],
  providers: [
    // Aggregates
    FrappeTokenManagerService,
    FrappeClientAggregateService,
    RequestLogManagerService,

    // Entity Repositories
    FrappeTokenService,
    RequestStateService,
    FrappeClientService,
    RequestLogService,

    // CQRS
    ...FrappeCommandHandlers,
    ...FrappeEventHandlers,
    ...FrappeQueryHandlers,
  ],
})
export class FrappeConnectorModule {}
