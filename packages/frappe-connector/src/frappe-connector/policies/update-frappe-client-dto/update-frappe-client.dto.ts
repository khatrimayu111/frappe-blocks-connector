import {
  IsString,
  IsUrl,
  IsNotEmpty,
  IsArray,
  IsOptional,
} from 'class-validator';

export class UpdateFrappeClientDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsUrl()
  authServerURL: string;

  @IsString()
  @IsNotEmpty()
  clientId: string;

  @IsString()
  @IsOptional()
  clientSecret: string;

  @IsUrl()
  profileURL: string;

  @IsUrl()
  tokenURL: string;

  @IsUrl()
  authorizationURL: string;

  @IsUrl()
  revocationURL: string;

  @IsArray()
  @IsNotEmpty({ each: true })
  scope: string[];
}
