import { Controller, UseGuards, Query, Get } from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { ListFrappeClientQuery } from '../../queries/list-frappe-client/list-frappe-client.query';

@Controller('frappe_token')
export class FrappeTokenController {
  constructor(private readonly queryBus: QueryBus) {}

  @Get('v1/list')
  @UseGuards(TokenGuard)
  async getClientList(
    @Query('offset') offset: number,
    @Query('limit') limit: number,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
  ) {
    return await this.queryBus.execute(
      new ListFrappeClientQuery(offset, limit, search, sort),
    );
  }
}
