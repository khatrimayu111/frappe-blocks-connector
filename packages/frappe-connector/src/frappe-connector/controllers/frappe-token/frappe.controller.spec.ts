import { Test, TestingModule } from '@nestjs/testing';
import { CqrsModule } from '@nestjs/cqrs';
import { FrappeTokenManagerService } from '../../aggregates/frappe-token-manager/frappe-token-manager.service';
import { TokenCacheService } from '../../../auth/entities/token-cache/token-cache.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { HttpService } from '@nestjs/common';
import { FrappeTokenController } from './frappe-token.controller';

describe('Frappe Token Controller', () => {
  let controller: FrappeTokenController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      controllers: [FrappeTokenController],
      providers: [
        { provide: FrappeTokenManagerService, useValue: {} },
        { provide: TokenCacheService, useValue: {} },
        { provide: ServerSettingsService, useValue: {} },
        { provide: HttpService, useFactory: (...args) => jest.fn() },
      ],
    }).compile();

    controller = module.get<FrappeTokenController>(FrappeTokenController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
