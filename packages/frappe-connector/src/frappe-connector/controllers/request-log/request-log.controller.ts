import {
  Controller,
  UseGuards,
  Query,
  Get,
  Param,
  Req,
  BadRequestException,
} from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { ListAdminRequestLogQuery } from '../../queries/list-request-log/list-request-log.query';
import { RetrieveRequestLogQuery } from '../../queries/retrieve-request-log/retrieve-request-log.query';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { ListUserRequestLogQuery } from '../../queries/list-user-request-log/list-user-request-log.query';
import { INVALID_USER } from '../../../constants/messages';

@Controller('request_log')
export class RequestLogController {
  constructor(private readonly queryBus: QueryBus) {}

  @Get('v1/list')
  @UseGuards(TokenGuard)
  async getClientList(
    @Req() req,
    @Query('offset') offset: number,
    @Query('limit') limit: number,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
  ) {
    if (req.token.roles && req.token.roles.includes(ADMINISTRATOR)) {
      return await this.queryBus.execute(
        new ListAdminRequestLogQuery(offset, limit, search, sort),
      );
    }
    if (req.token.sub) {
      return await this.queryBus.execute(
        new ListUserRequestLogQuery(offset, limit, req.token.sub, search, sort),
      );
    }
    throw new BadRequestException(INVALID_USER);
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getRequestLog(@Param('uuid') uuid) {
    return await this.queryBus.execute(new RetrieveRequestLogQuery(uuid));
  }
}
