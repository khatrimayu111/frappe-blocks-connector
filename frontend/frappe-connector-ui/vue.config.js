// vue.config.js
module.exports = {
    devServer: {
        proxy: {
            "/info": { "target": "http://fcon.localhost:3300", "secure": false, "logLevel": "debug" },
            "/frappe/v1": { "target": "http://fcon.localhost:3300", "secure": false, "logLevel": "debug" },
            "/frappe/callback": { "target": "http://fcon.localhost:3300", "secure": false, "logLevel": "debug" },
            "/request_log/v1": { "target": "http://fcon.localhost:3300", "secure": false, "logLevel": "debug" },
            "/settings/v1": { "target": "http://fcon.localhost:3300", "secure": false, "logLevel": "debug" },
            "/connect/v1": { "target": "http://fcon.localhost:3300", "secure": false, "logLevel": "debug" },
            "/setup": { "target": "http://fcon.localhost:3300", "secure": false, "logLevel": "debug" },
        },
    }
}
