import Axios from 'axios';
import { ACCESS_TOKEN, APP_URL } from '../../constants/storage';

async function findModels(
  model: string,
  filters = '',
  sortOrder = 'ASC',
  pageNumber = 0,
  pageSize = 10,
  query = 'id > 0',
) {
  const authorizationHeader = 'Bearer ' + localStorage.getItem(ACCESS_TOKEN);
  const params =
    '?limit=' +
    pageSize.toString() +
    '&offset=' +
    (pageNumber * pageSize).toString() +
    '&search=' +
    filters +
    '&sort=' +
    sortOrder +
    '&filters=' +
    query;
  const url = localStorage.getItem(APP_URL) + `/${model}/v1/list` + params;
  return await Axios.get(url, {
    headers: { authorization: `${authorizationHeader}` },
  });
}

export { findModels };
