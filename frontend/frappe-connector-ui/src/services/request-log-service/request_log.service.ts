import Axios from 'axios';
import * as frappe from '../frappe-client/frappe.service';

function getRequestLogs(uuid) {
  return Axios.get('/request_log/v1/get/' + uuid, {
    headers: frappe.getAuthorizationHeaders(),
  });
}

export { getRequestLogs };
