import Axios from 'axios';
import { ACCESS_TOKEN } from '../../constants/storage';

async function getFrappeClient(uuid) {
  return Axios.get('/frappe/v1/get/' + uuid, {
    headers: getAuthorizationHeaders(),
  });
}

function getAuthorizationHeaders() {
  return { authorization: 'Bearer ' + localStorage.getItem(ACCESS_TOKEN) };
}

function createFrappeClient(
  name: string,
  authorizationURL: string,
  clientId: string,
  clientSecret: string,
  profileURL: string,
  revocationURL: string,
  scope: string[],
  authServerURL: string,
  tokenURL: string,
) {
  const data = {
    name,
    authorizationURL,
    clientId,
    clientSecret,
    profileURL,
    revocationURL,
    scope,
    authServerURL,
    tokenURL,
  };
  return Axios.post('/frappe/v1/connect_client', data, {
    headers: getAuthorizationHeaders(),
  });
}

function updateFrappeClient(
  uuid: string,
  name: string,
  authorizationURL: string,
  clientId: string,
  clientSecret: string,
  profileURL: string,
  revocationURL: string,
  scope: string[],
  authServerURL: string,
  tokenURL: string,
) {
  const data = {
    name,
    authorizationURL,
    clientId,
    clientSecret,
    profileURL,
    revocationURL,
    scope,
    authServerURL,
    tokenURL,
  };
  return Axios.post('/frappe/v1/update_client/' + uuid, data, {
    headers: getAuthorizationHeaders(),
  });
}

export {
  getFrappeClient,
  getAuthorizationHeaders,
  createFrappeClient,
  updateFrappeClient,
};
