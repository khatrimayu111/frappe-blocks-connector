import Router from 'vue-router';
import Home from './views/Home.vue';
import Callback from './views/Callback.vue';
import Listing from './views/Listing.vue';
import Client from './views/Client.vue';
import Request_log from './views/Request_log.vue';

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/callback',
      name: 'Callback',
      component: Callback,
    },
    {
      path: '/listing/:model',
      name: 'Listing',
      component: Listing,
    },
    {
      path: '/frappe/:uuid',
      name: 'Frappe',
      component: Client,
    },
    {
      path: '/request_log/:uuid',
      name: 'Request-log',
      component: Request_log,
    },
  ],
});
