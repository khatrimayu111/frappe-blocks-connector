#!/bin/bash

cd /tmp

# Clone building blocks configuration
git clone https://github.com/castlecraft/helm-charts

# Check helm chart is installed or create
# reuse installed values and resets data

export CHECK_FC=$(helm ls -q staging-frappe-blocks-connector --tiller-namespace building-blocks-7336983)
if [ "$CHECK_FC" = "staging-frappe-blocks-connector" ]
then
    echo "Updating existing staging-frappe-blocks-connector . . ."
    helm upgrade staging-frappe-blocks-connector \
        --tiller-namespace building-blocks-7336983 \
        --namespace building-blocks-7336983 \
        --reuse-values \
        --recreate-pods \
        helm-charts/frappe-blocks-connector
fi
